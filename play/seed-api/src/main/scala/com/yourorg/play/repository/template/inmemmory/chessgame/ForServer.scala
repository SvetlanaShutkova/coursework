package com.yourorg.play.repository.template.inmemmory.chessgame


case class ForServer(board: Seq[Seq[PieceInput]], castledWhite: Boolean = false,
                     castledBlack: Boolean = false,
                     turn: Int = 1,
                     finished: Boolean = false)

