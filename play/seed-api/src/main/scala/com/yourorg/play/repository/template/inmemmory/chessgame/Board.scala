package com.yourorg.play.repository.template.inmemmory.chessgame

case class Board(startPosition: Seq[Seq[Cell]]) {
  val board: Seq[Seq[Cell]] = startPosition

  def getCell(x: Int, y: Int): Cell = board.apply(x).apply(y)

  def getBoard: Seq[Seq[Cell]] = board

  def getKing(color: String): Cell =
    board
      .map(row => row map (x => if (x.piece.isInstanceOf[King] && x.piece.color == color) x else Cell(-1, -1)))
      .flatMap(x => x.filter(c => c.x != -1))
      .head

  def printBoard(): Unit = {
    board foreach { x =>
      x foreach { y =>
        print(y.piece + " ")
      }; println
    }; println
  }

  def updateBoard(curCell: Cell, newCell: Cell): Board = {
    val tempBoard = Board(
      board
        .updated(newCell.x, board.apply(newCell.x).updated(newCell.y, newCell.copy(newPiece = curCell.piece)))
    )
    Board(
      tempBoard.board
        .updated(curCell.x, tempBoard.board.apply(curCell.x).updated(curCell.y, curCell.copy(newPiece = EmptySpace())))
    )
  }
  def castle(curCell: Cell, newCell: Cell): Board = {
    val newKingCell = if (newCell.y == 0) getCell(newCell.x, 2) else getCell(curCell.x, 6)
    val newRookCell = if (newCell.y == 0) getCell(newCell.x, 3) else getCell(curCell.x, 5)
    val newSeq = board.apply(newRookCell.x)
      .updated(newRookCell.y, newRookCell.copy(newPiece = newCell.piece))
      .updated(newCell.y, newCell.copy(newPiece = EmptySpace()))
      .updated(newKingCell.y, newKingCell.copy(newPiece = curCell.piece))
      .updated(curCell.y, curCell.copy(newPiece = EmptySpace()))
    Board(board.updated(newRookCell.x, newSeq))

  }
  def convertFromPieceInput(boardPieceInput: Seq[Seq[PieceInput]]): Board =
    Board(
      boardPieceInput
        .map(x => x.map(y => y.inputToPiece(y)))
        .zipWithIndex
        .map(row => (row._1.zipWithIndex, row._2))
        .map(x => x._1 map (y => getCell(x._2, y._2)))
    )
}
