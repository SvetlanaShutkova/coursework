package com.yourorg.play
package http
package endpoints

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import com.typesafe.scalalogging.LazyLogging
import com.yourorg.play.http.auth.Authenticable
import com.yourorg.play.repository.template.inmemmory.chessgame._
import com.yourorg.play.util.cfg.Configurable
import com.yourorg.play.util.json.FailFastCirceSupport
import io.circe.generic.auto._
import tapir.json.circe._
import tapir.server.akkahttp._
import tapir.{Endpoint, endpoint, jsonBody, _}

import scala.concurrent.{ExecutionContext, Future}

class Routes(newGame: Game)(implicit ec: ExecutionContext)
  extends Configurable with Authenticable with FailFastCirceSupport with LazyLogging with RouteConcatenation {

  import akka.http.scaladsl.server.{Directives => directive}

  private val basePath = "api"
  private val version = "v1"

  val route = getBoardRoute ~ addMoveRoute

  private def state: Future[ForServer] =
    Future(ForServer(newGame.convertInputPiece, newGame.castledWhite,
      newGame.castledBlack, newGame.turn, newGame.finished))

  private def move1: Future[ForServer] =
    state.map(x =>
      ForServer(Game(newGame.chessBoard.convertFromPieceInput(x.board)).move((7, 1), (5, 2)).convertInputPiece))

  val getBoardEndpoint: Endpoint[String, Error, ForServer, Nothing] =
    endpoint.get
      .in(basePath / version / "state")
      .in(path[String]("id"))
      .out(jsonBody[ForServer])
      .errorOut(jsonBody[Error])

  def getBoardRoute: Route = directive.get {
      getBoardEndpoint.toRoute(_ => state.map(Right(_))
        .recover {
          case e: Throwable =>
            logger.error("Error occurred during loading board", e)
            Left(Error(StatusCodes.InternalServerError.intValue, "api is not available"))
        })
  }

  val addMoveEndpoint: Endpoint[ForServer, Error, ForServer, Nothing] =
    endpoint.post
      .in(basePath / version / "state")
      .in(jsonBody[ForServer])
      .out(jsonBody[ForServer])
      .errorOut(jsonBody[Error])

  def addMoveRoute: Route = directive.post {
      addMoveEndpoint.toRoute(_ => move1.map(Right(_))
        .recover {
          case e: Throwable =>
            logger.error("Error occurred during loading board", e)
            Left(Error(StatusCodes.InternalServerError.intValue, "api is not available"))
        })
  }
}






