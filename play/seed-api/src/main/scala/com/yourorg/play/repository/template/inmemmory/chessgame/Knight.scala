package com.yourorg.play.repository.template.inmemmory.chessgame

case class Knight(myColor: String) extends Piece {
  override val color: String = myColor
  override val available: Boolean = true
  def isValidMove(currentCell: Cell, newCell: Cell, board: Board): Boolean = {
    def knightDistance: Double =
      math.sqrt(math.pow(currentCell.x - newCell.x, 2) + math.pow(currentCell.y - newCell.y, 2))
    if (newCell.piece.color == color) {
      false
    } else {
      knightDistance == math.sqrt(5)
    }
  }

  override def getProtection(currentCell: Cell, board: Board): Seq[Cell] = {
    def exists(x: Int, y: Int): Boolean = x >= 0 && x < 8 && y >= 0 && y < 8
    val protectedCells =
      for { (i, j) <- Seq((-2, 1), (-2, -1), (-1, -2), (-1, 2), (1, 2), (1, -2), (2, 1), (2, -1)) } yield {
        if (exists(currentCell.x + i, currentCell.y + j)) {
          board.getCell(currentCell.x + i, currentCell.y + j)
        } else {
          Cell(-1, -1)
        }
      }
    protectedCells.filter(cell => cell.x != -1)
  }

  override def toString: String = s"N"
}
