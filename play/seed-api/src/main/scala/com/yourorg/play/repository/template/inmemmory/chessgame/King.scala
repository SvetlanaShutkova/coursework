package com.yourorg.play.repository.template.inmemmory.chessgame

case class King(myColor: String) extends Piece {
  override val color: String = myColor

  override val available: Boolean = true
  def isValidMove(currentCell: Cell, newCell: Cell, board: Board): Boolean =
    if (newCell.piece.color == color && newCell.piece.isInstanceOf[Rook]) {
        val range = if (currentCell.y > newCell.y) {
          { newCell.y to currentCell.y } map (y => (currentCell.x, y))
        } else {
          currentCell.y to newCell.y map (y => (currentCell.x, y))
        }
        val path = for { (i, j) <- range } yield { board.getCell(i, j) }
        val temp = path
          .drop(1)
          .dropRight(1)
          .filter(
            cell =>
              cell.isOccupied || (if (currentCell.piece.color == "white" && cell.protectedByBlack)
                                    true
                                  else (currentCell.piece.color == "black" && cell.protectedByWhite))
          )
        temp.isEmpty
    } else {
      math.abs(currentCell.x - newCell.x) <= 1 && math.abs(currentCell.y - newCell.y) <= 1
    }

  override def getProtection(currentCell: Cell, board: Board): Seq[Cell] = {
    def exists(x: Int, y: Int): Boolean = x >= 0 && x < 8 && y >= 0 && y < 8
    val protectedCells =
      for { (i, j) <- Seq((1, 1), (1, -1), (1, 0), (0, 1), (0, -1), (-1, 0), (-1, -1), (-1, 1)) } yield {
        if (exists(currentCell.x + i, currentCell.y + j)) {
          board.getCell(currentCell.x + i, currentCell.y + j)
        } else {
          Cell(-1, -1)
        }
      }
    protectedCells.filter(cell => cell.x != -1)
  }
  override def toString: String = s"K"
}
