package com.yourorg.play.repository.template.inmemmory.chessgame

case class Player(name: String){
  val numWins = 0
  val numLoss = 0
  val numDraws = 0
}