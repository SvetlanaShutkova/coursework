package com.yourorg.play.repository.template.inmemmory.chessgame

case class Rook(myColor: String) extends Piece {
  override val color: String = myColor

  override val available: Boolean = true

  def isValidMove(currentCell: Cell, newCell: Cell, board: Board): Boolean =
    if (newCell.piece.color == color) {
      false
    } else {
      isValidProtection(currentCell, newCell, board)
    }

  def isValidProtection(currentCell: Cell, newCell: Cell, board: Board): Boolean =
    if (currentCell.x == newCell.x) {
      val rangeY = if (currentCell.y < newCell.y) {
        currentCell.y to newCell.y
      } else {
        currentCell.y to newCell.y by -1
      }
      val myBools = for { y <- rangeY } yield {
        board.getCell(currentCell.x, y).isOccupied
      }
      !myBools
        .dropRight(1)
        .drop(1)
        .contains(true)
    } else if (currentCell.y == newCell.y) {
      val rangeX = if (currentCell.x < newCell.x) {
        currentCell.x to newCell.x
      } else {
        currentCell.x to newCell.x by -1
      }
      val myBools = for { x <- rangeX } yield {
        board.getCell(x, currentCell.y).isOccupied
      }
      !myBools
        .dropRight(1)
        .drop(1)
        .contains(true)
    } else {
      false
    }

  override def getProtection(currentCell: Cell, board: Board): Seq[Cell] = {
    def exists(x: Int, y: Int): Boolean = x >= 0 && x < 8 && y >= 0 && y < 8

    val range = {
      (-7 to 7) map (x => (0, x))
    } ++ {
      (-7 to 7) map (x => (x, 0))
    }
    val protectedCells =
      for { (i, j) <- range } yield {
        if (exists(currentCell.x + i, currentCell.y + j)) {
          board.getCell(currentCell.x + i, currentCell.y + j)
        } else {
          Cell(-1, -1)
        }
      }
    protectedCells
      .filter(cell => exists(cell.x, cell.y))
      .filter(cell => isValidProtection(currentCell, cell, board))
      .distinct
  }

  override def toString: String = s"R"
}
