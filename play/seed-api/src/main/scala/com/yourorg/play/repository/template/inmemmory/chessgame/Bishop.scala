package com.yourorg.play.repository.template.inmemmory.chessgame

case class Bishop(myColor: String) extends Piece {
  override val color: String = myColor
  override val available: Boolean = true
  def isValidMove(currentCell: Cell, newCell: Cell, board: Board): Boolean =
    if (newCell.piece.color == color) {false}
    else {
      isPathClear(currentCell, newCell, board)
    }

  def isPathClear(currentCell: Cell, newCell: Cell, board: Board): Boolean =
    if (math.abs(currentCell.x - newCell.x) != math.abs(currentCell.y - newCell.y)) {false}
    else {
      val rangeX = if (currentCell.x < newCell.x) currentCell.x to newCell.x else currentCell.x to newCell.x by -1
      val rangeY = if (currentCell.y < newCell.y) currentCell.y to newCell.y else currentCell.y to newCell.y by -1
      val myBools = {
        for {(first, second) <- rangeX zip rangeY} yield { board.getCell(first, second).isOccupied }
      }
      !myBools
        .dropRight(1)
        .drop(1)
        .contains(true)
    }

  override def getProtection(currentCell: Cell, board: Board): Seq[Cell] = {
    def exists(x: Int, y: Int): Boolean = x >= 0 && x < 8 && y >= 0 && y < 8
    val range = { (-7 to 7) map (x => (x, x)) } ++ { (-7 to 7) map (x => (-x, x)) }
    val protectedCells =
      for {(i, j) <- range}
        yield {
          if (exists(currentCell.x + i, currentCell.y + j)) {board.getCell(currentCell.x + i, currentCell.y + j)}
          else {Cell(-1, -1)}
        }
    protectedCells
      .filter(cell => exists(cell.x, cell.y))
      .filter(cell => isPathClear(currentCell, cell, board))
      .distinct
  }

  override def toString: String = s"B"
}
