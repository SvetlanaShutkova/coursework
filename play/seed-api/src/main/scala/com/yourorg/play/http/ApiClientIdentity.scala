package com.yourorg.play.http

/**
 * Used for authentication of the API client.
 */
final case class ApiClientIdentity(
  clientId: String
)
