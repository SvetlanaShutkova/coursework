package com.yourorg.play.repository.template.inmemmory.chessgame

case class PieceInput(typePiece: String, color: String) {

  def inputToPiece(input: PieceInput): Piece = {
    input match {
      case PieceInput("Bishop", color) => Bishop(color)
      case PieceInput("King", color) => King(color)
      case PieceInput("Knight", color) => Knight(color)
      case PieceInput("Pawn", color) => Pawn(color)
      case PieceInput("Queen", color) => Queen(color)
      case PieceInput("Rook", color) => Rook(color)
      case PieceInput("EmptySpace", _) => EmptySpace()
      case x => {println(x); EmptySpace()}
    }
  }

}