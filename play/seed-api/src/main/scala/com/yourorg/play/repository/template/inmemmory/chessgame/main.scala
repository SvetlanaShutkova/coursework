package com.yourorg.play.repository.template.inmemmory.chessgame
import scala.util.matching.Regex

object main {
  def getCoordinates(pattern: Regex): Array[Int] = {
    var move = (pattern findFirstIn scala.io.StdIn.readLine()).getOrElse("Invalid move")
    while (move == "Invalid move") {
      println("Invalid coordinate")
      move = (pattern findFirstIn scala.io.StdIn.readLine()).getOrElse("Invalid move")
    }
    move.split(" ").map(x => x.toInt)
  }

  /**
   * система координат для доски:
   *   0 1 2 3 4 5 6 7
   * 0
   * 1
   * 2
   * 3
   * 4
   * 5
   * 6
   * 7
   * Формат ввода хода: x y
   * где x - это строка, y - столбец
   */

  def main(args: Array[String]): Unit = {
    val chessGame = Game(player1 = Player("Player1"), player2 = Player("Player2"))
    val historyStates = Seq(chessGame)
    val pattern = new Regex("\\d \\d")

    val stream = Stream
      .iterate(historyStates)(x => {

        val moveFrom = getCoordinates(pattern)
        val moveTo = getCoordinates(pattern)

        x :+ x.last.move((moveFrom(0), moveFrom(1)), (moveTo(0), moveTo(1)))
      })
      .takeWhile(x => !x.last.finished)
    for (state <- stream) { state.last.chessBoard.printBoard(); }
  }
}