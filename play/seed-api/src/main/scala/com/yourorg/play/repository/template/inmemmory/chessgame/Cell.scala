package com.yourorg.play.repository.template.inmemmory.chessgame


case class Cell(x: Int, y: Int, piece: Piece = new EmptySpace,
                protectedByWhite: Boolean = false, protectedByBlack: Boolean = false) {

  def isOccupied: Boolean = this.piece.available

  def isNotOccupied: Boolean = !isOccupied

  def copy(x: Int = x, y: Int = y, newPiece: Piece = piece, protectedByWhite: Boolean = false,
           protectedByBlack: Boolean = false): Cell = Cell(x, y, newPiece, protectedByWhite, protectedByBlack)

  def occupy(piece: Piece): Cell = copy(x, y, piece)

  def release(): Cell = copy(newPiece = new EmptySpace)


  override def toString: String = s"($x, $y, $piece, $isOccupied)"
}