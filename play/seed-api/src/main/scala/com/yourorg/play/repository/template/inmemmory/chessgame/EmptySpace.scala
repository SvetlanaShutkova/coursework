package com.yourorg.play.repository.template.inmemmory.chessgame


case class EmptySpace() extends Piece {
  val color: String = "None"
  val available: Boolean = false

  override def isValidMove(currentCell: Cell, newCell: Cell, board: Board): Boolean = false

  override def getProtection(currentCell: Cell, board: Board): Seq[Cell] = Seq()

  override def toString: String = s"_"
}
