package com.yourorg.play.repository.template.inmemmory.chessgame

case class Game(
  chessBoard: Board = Board(
    Seq(
      Seq(
        Cell(0, 0, Rook("black")),
        Cell(0, 1, Knight("black")),
        Cell(0, 2, Bishop("black")),
        Cell(0, 3, Queen("black")),
        Cell(0, 4, King("black")),
        Cell(0, 5, Bishop("black")),
        Cell(0, 6, Knight("black")),
        Cell(0, 7, Rook("black"))
      ),
      Seq(
        Cell(1, 0, Pawn("black")),
        Cell(1, 1, Pawn("black")),
        Cell(1, 2, Pawn("black")),
        Cell(1, 3, Pawn("black")),
        Cell(1, 4, Pawn("black")),
        Cell(1, 5, Pawn("black")),
        Cell(1, 6, Pawn("black")),
        Cell(1, 7, Pawn("black"))
      ),
      Seq(
        Cell(2, 0, EmptySpace()),
        Cell(2, 1, EmptySpace()),
        Cell(2, 2, EmptySpace()),
        Cell(2, 3, EmptySpace()),
        Cell(2, 4, EmptySpace()),
        Cell(2, 5, EmptySpace()),
        Cell(2, 6, EmptySpace()),
        Cell(2, 7, EmptySpace())
      ),
      Seq(
        Cell(3, 0, EmptySpace()),
        Cell(3, 1, EmptySpace()),
        Cell(3, 2, EmptySpace()),
        Cell(3, 3, EmptySpace()),
        Cell(3, 4, EmptySpace()),
        Cell(3, 5, EmptySpace()),
        Cell(3, 6, EmptySpace()),
        Cell(3, 7, EmptySpace())
      ),
      Seq(
        Cell(4, 0, EmptySpace()),
        Cell(4, 1, EmptySpace()),
        Cell(4, 2, EmptySpace()),
        Cell(4, 3, EmptySpace()),
        Cell(4, 4, EmptySpace()),
        Cell(4, 5, EmptySpace()),
        Cell(4, 6, EmptySpace()),
        Cell(4, 7, EmptySpace())
      ),
      Seq(
        Cell(5, 0, EmptySpace()),
        Cell(5, 1, EmptySpace()),
        Cell(5, 2, EmptySpace()),
        Cell(5, 3, EmptySpace()),
        Cell(5, 4, EmptySpace()),
        Cell(5, 5, EmptySpace()),
        Cell(5, 6, EmptySpace()),
        Cell(5, 7, EmptySpace())
      ),
      Seq(
        Cell(6, 0, Pawn("white")),
        Cell(6, 1, Pawn("white")),
        Cell(6, 2, Pawn("white")),
        Cell(6, 3, Pawn("white")),
        Cell(6, 4, Pawn("white")),
        Cell(6, 5, Pawn("white")),
        Cell(6, 6, Pawn("white")),
        Cell(6, 7, Pawn("white"))
      ),
      Seq(
        Cell(7, 0, Rook("white")),
        Cell(7, 1, Knight("white")),
        Cell(7, 2, Bishop("white")),
        Cell(7, 3, Queen("white")),
        Cell(7, 4, King("white")),
        Cell(7, 5, Bishop("white")),
        Cell(7, 6, Knight("white")),
        Cell(7, 7, Rook("white"))
      )
    )
  ),
  castledBlack: Boolean = false,
  castledWhite: Boolean = false,
  turn: Int = 1,
  finished: Boolean = false,
  player1: Player = Player("1"),
  player2: Player = Player("2")
) {
  def convertInputPiece: Seq[Seq[PieceInput]] = chessBoard.board map { x =>
    x map { y =>
      PieceInput(y.piece.toString, y.piece.color)
    }
  }

  def calculateProtection(): Board = {
    val protectedCells = chessBoard.board.flatMap { row =>
      row map { cell =>
        cell.piece.getProtection(cell, chessBoard) map { protectedCell =>
          chessBoard
            .getCell(protectedCell.x, protectedCell.y)
            .copy(protectedByWhite = cell.piece.color == "white", protectedByBlack = cell.piece.color == "black")
        }
      }
    }.flatten

    protectedCells.foldLeft(chessBoard)(
      (accBoard, protectedCell) =>
        Board(
          accBoard.board
            .updated(
              protectedCell.x,
              accBoard.board
                .apply(protectedCell.x)
                .updated(
                  protectedCell.y,
                  accBoard
                    .getCell(protectedCell.x, protectedCell.y)
                    .copy(
                      protectedByWhite = protectedCell.protectedByWhite || accBoard
                        .getCell(protectedCell.x, protectedCell.y)
                        .protectedByWhite,
                      protectedByBlack = protectedCell.protectedByBlack || accBoard
                        .getCell(protectedCell.x, protectedCell.y)
                        .protectedByBlack
                    )
                )
            )
        )
    )
  }

  def moveIsNotPossible(curCord: (Int, Int), newCord: (Int, Int)): Boolean =
    (curCord._1 > 7 || curCord._1 < 0 || newCord._2 > 7 || newCord._2 < 0) ||
    !((turn % 2 != 0 && this.chessBoard.getCell(curCord._1, curCord._2).piece.color == "white") ||
      (turn % 2 == 0 && this.chessBoard.getCell(curCord._1, curCord._2).piece.color == "black"))

  def move(curCord: (Int, Int), newCord: (Int, Int)): Game =
    if (moveIsNotPossible(curCord, newCord)){
      println("Invalid move"); this
    } else {
      val currentColor = this.chessBoard.getCell(curCord._1, curCord._2).piece.color
      val curCell = this.chessBoard.getCell(curCord._1, curCord._2)
      val newCell = this.chessBoard.getCell(newCord._1, newCord._2)
      if (curCell.piece.isValidMove(curCell, newCell, this.chessBoard)) {

        val updatedGame = if (curCell.piece.isInstanceOf[King] && newCell.piece.isInstanceOf[Rook] &&
          (if (currentColor == "white") !castledWhite else !castledBlack)) {
          Game(
            this.chessBoard.castle(curCell, newCell),
            castledWhite = if (currentColor == "white") true else this.castledWhite,
            castledBlack = if (currentColor == "black") true else this.castledBlack,
            turn = this.turn + 1,
            finished = this.finished,
            player1 = this.player1,
            player2 = this.player2
          )
        }
        else {
          Game(
            chessBoard.updateBoard(curCell, newCell),
            castledWhite =  this.castledWhite,
            castledBlack =  this.castledBlack,
            turn = this.turn + 1,
            finished = this.finished,
            player1 = this.player1,
            player2 = this.player2
          )
        }
        val updatedGameProtections = updatedGame.calculateProtection()
        val enemyColor = if (turn % 2 == 0) "white" else "black"
        val myKingCell = updatedGameProtections.getKing(color = currentColor)
        if (currentColor == "white" && myKingCell.protectedByBlack
            || currentColor == "black" && myKingCell.protectedByWhite) {
          println("Invalid move")
          this
        } else if (isCheckmate(enemyColor, updatedGameProtections)) {println("Checkmate!");
          updatedGame.copy(chessBoard = updatedGameProtections, finished = true)}
        else{
          updatedGame.copy(chessBoard = updatedGameProtections)
        }
      } else {
        println("Invalid move"); this
      }
    }
  def isCheck(color: String, boardProtected: Board): Boolean =
    if (color == "white") {
      boardProtected.getKing(color).protectedByBlack
    } else {
      boardProtected.getKing(color).protectedByWhite
    }
  def isCheckmate(color: String, boardProtected: Board): Boolean = {
    val myKingCell = boardProtected.getKing(color)
    isCheck(color, boardProtected) && myKingCell.piece
      .getProtection(myKingCell, boardProtected)
      .filter(cell => cell.piece.color != color)
      .filter(
        cell =>
          cell.piece.available &&
            ! {
              if (color == "white") {
                cell.protectedByBlack
              } else {
                cell.protectedByWhite
              }
            }
      )
      .forall(cell => {
        if (color == "white") {
          cell.protectedByBlack
        } else {
          cell.protectedByWhite
        }
      })
  }
}
