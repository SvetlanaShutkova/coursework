package com.yourorg.play.repository.template.inmemmory.chessgame

case class Pawn(myColor: String) extends Piece {
  val isMoved = false
  override val color: String = myColor
  override val available: Boolean = true

  override def isValidMove(currentCell: Cell, newCell: Cell, board: Board): Boolean = {

    def moved(num: Int): Boolean =
      (currentCell.x - newCell.x == num && color == "white" ||
        newCell.x - currentCell.x == num && color == "black") && currentCell.y == newCell.y

    def capture: Boolean =
      (currentCell.x - newCell.x == 1 && color == "white" && newCell.piece.color == "black" ||
        newCell.x - currentCell.x == 1 && color == "black" && newCell.piece.color == "white") &&
        math.abs(currentCell.y - newCell.y) == 1

    def pathIsClear: Boolean =
      board.getCell(currentCell.x - 1, currentCell.y).isNotOccupied && color == "white" ||
        board.getCell(currentCell.x + 1, currentCell.y).isNotOccupied && color == "black"

    if (newCell.piece.color == color) {
      false
    } else {
      if (moved(1) || capture) {
        true
      } else (moved(2) && !isMoved && pathIsClear)
    }
  }

  override def getProtection(currentCell: Cell, board: Board): Seq[Cell] =
    if (currentCell.piece.color == "white" && currentCell.x > 0) {
      if (currentCell.y > 0 && currentCell.y < 7)
        Seq(board.getCell(currentCell.x - 1, currentCell.y - 1), board.getCell(currentCell.x - 1, currentCell.y + 1))
      else if (currentCell.y == 0) Seq(board.getCell(currentCell.x - 1, currentCell.y + 1))
      else if (currentCell.y == 7) Seq(board.getCell(currentCell.x - 1, currentCell.y - 1))
      else Seq()
    } else if (currentCell.piece.color == "black" && currentCell.x < 7) {
      if (currentCell.y > 0 && currentCell.y < 7)
        Seq(board.getCell(currentCell.x + 1, currentCell.y - 1), board.getCell(currentCell.x + 1, currentCell.y + 1))
      else if (currentCell.y == 0) Seq(board.getCell(currentCell.x + 1, currentCell.y + 1))
      else if (currentCell.y == 7) Seq(board.getCell(currentCell.x + 1, currentCell.y - 1))
      else Seq()
    } else Seq()

  override def toString: String = s"P"
}
