package com.yourorg.play.repository.template.inmemmory.chessgame

trait Piece {
  val color: String
  val available: Boolean

  def isValidMove(currentCell: Cell, newCell: Cell, board: Board): Boolean
  def getProtection(currentCell: Cell, board: Board): Seq[Cell]
}
