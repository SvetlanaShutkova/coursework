package com.yourorg.play.repository.template.inmemmory.chessgame

import slick.jdbc.H2Profile.api._
import slick.lifted.{ProvenShape, Rep}


case class ChessData(
  gameId: Int,
  gameData: String
)

class ChessDataQuery(tag: Tag) extends Table[ChessData](tag, "CHESS_GAMES") {

  def gameId: Rep[Int] = column("GAME_ID", O.PrimaryKey)

  def gameData: Rep[String] = column("GAME_DATA")

  override def * : ProvenShape[ChessData] = (gameId, gameData).mapTo[ChessData]
}
