package com.yourorg.play
package http

final case class Error(code: Int, message: String)
