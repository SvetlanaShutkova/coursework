package com.yourorg.play

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives.{handleExceptions, handleRejections}
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import akka.stream.ActorMaterializer
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import http.swagger.SwaggerRoutes
import com.typesafe.scalalogging.LazyLogging
import com.yourorg.play.repository.template.inmemmory.chessgame.Game

import scala.concurrent.{ExecutionContext, Future}
import tapir.docs.openapi._
import tapir.openapi.circe.yaml._


object Starter
    extends App with RouteConcatenation
    with util.cfg.Configurable
    with util.http.ExceptionHandler
    with util.http.RejectionHandler
    with LazyLogging {

  def startApplication(): Future[Http.ServerBinding] = {

    implicit val actorSystem: ActorSystem = ActorSystem()
    implicit val executor: ExecutionContext = actorSystem.dispatcher
    implicit val materializer: ActorMaterializer = ActorMaterializer()(actorSystem)

    val routeGame: http.endpoints.Routes = new http.endpoints.Routes(newGame = Game())

    val games: Route = routeGame.route

    val swagger: Route = {
      val Yaml = List(
        routeGame.getBoardEndpoint,
        routeGame.addMoveEndpoint
      ).toOpenAPI("Template API", "1.0").toYaml
      new SwaggerRoutes(Yaml).routes
    }

    val routes: Route = handleRejections(rejectionHandler) {
      handleExceptions(exceptionHandler) {
        cors() {
          games ~ swagger
        }
      }
    }

    Http().bindAndHandle(routes, config.http.host, config.http.port)
  }

  startApplication()

}
