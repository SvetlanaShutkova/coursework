import com.yourorg.play.repository.template.inmemmory.chessgame.{Bishop, EmptySpace, Game, Knight, Pawn, Rook, King, Queen}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class chesslogic extends AnyFunSuite with Matchers{
  val startPosition = new Game

  test("Ход пешкой в пределах поля") {
    startPosition.move((6, 4),(4, 4)).chessBoard.getCell(6, 4).piece shouldBe EmptySpace()
    startPosition.move((6, 4),(4, 4)).chessBoard.getCell(4, 4).piece shouldBe Pawn("white")
  }

  test("Ход конем в пределах поля"){
    startPosition.move((7, 1), (5, 2)).chessBoard.getCell(7, 1).piece shouldBe EmptySpace()
    startPosition.move((7, 1), (5, 2)).chessBoard.getCell(5, 2).piece shouldBe Knight("white")
  }

  test("Ход слоном в пределах поля") {
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4))
      .move((7, 5),(4, 2)).chessBoard.getCell(7, 5).piece shouldBe EmptySpace()
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4))
      .move((7, 5),(4, 2)).chessBoard.getCell(4, 2).piece shouldBe Bishop("white")
  }

  test("Ход ладьей в пределах поля") {
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7))
      .move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .move((1, 0),(2, 0)).move((7, 5),(7, 4))
      .chessBoard.getCell(7, 5).piece shouldBe EmptySpace()
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7))
      .move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .move((1, 0),(2, 0)).move((7, 5),(7, 4))
      .chessBoard.getCell(7, 4).piece shouldBe Rook("white")
  }

  test("Ход королевы") {
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3))
      .move((7, 4),(7, 7)).move((1, 0),(2, 0)).move((7, 3),(7, 4))
      .chessBoard.getCell(7, 3).piece shouldBe EmptySpace()
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3))
      .move((7, 4),(7, 7)).move((1, 0),(2, 0)).move((7, 3),(7, 4))
      .chessBoard.getCell(7, 4).piece shouldBe Queen("white")
  }

  test("Ход короля") {
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 5))
      .chessBoard.getCell(7, 4).piece shouldBe EmptySpace()
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 5))
      .chessBoard.getCell(7, 5).piece shouldBe King("white")
  }

  test("Взятие фигуры") {
    startPosition.move((6, 4),(4, 4)).move((1, 3),(3, 3)).move((7, 5),(4, 2)).move((3, 3),(4, 4))
      .chessBoard.getCell(3, 3).piece shouldBe EmptySpace()
    startPosition.move((6, 4),(4, 4)).move((1, 3),(3, 3)).move((7, 5),(4, 2)).move((3, 3),(4, 4))
      .chessBoard.getCell(4, 4).piece shouldBe Pawn("black")
  }


  test("Ракировка белых") {
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .chessBoard.getCell(7, 5).piece shouldBe Rook("white")
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .chessBoard.getCell(7, 6).piece shouldBe King("white")
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .castledWhite shouldBe true
  }
  test("Ракировка черных") {
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .move((0, 4),(0, 7))
      .chessBoard.getCell(0, 5).piece shouldBe Rook("black")
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .move((0, 4),(0, 7))
      .chessBoard.getCell(0, 6).piece shouldBe King("black")
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .move((0, 4),(0, 7))
      .castledBlack shouldBe true
  }

  test("Логика шаха") {
    startPosition.isCheck("black", startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4))
      .move((7, 5),(4, 2)).move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3))
      .move((7, 4),(7, 7)).move((1, 0),(2, 0)).move((7, 3),(3, 7)).move((1, 1),(3, 1))
      .move((3, 7),(1, 5)).chessBoard) shouldBe true
  }

  test("Логика мата") {
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4)).move((7, 5),(4, 2))
      .move((0, 6),(2, 5)).move((7, 6),(5, 7)).move((0, 5),(2, 3)).move((7, 4),(7, 7))
      .move((1, 0),(2, 0)).move((7, 3),(3, 7)).move((1, 1),(3, 1)).move((3, 7),(1, 5))
      .finished shouldBe true
  }

  test("Неправильная очередь хода") {
    startPosition.move((6, 4), (4, 4)).move((7, 5), (4, 2)) shouldBe startPosition.move((6, 4), (4, 4))
  }

  test("Ход за пределами поля") {
    startPosition.move((8, 9), (9, 9)) shouldBe startPosition
  }

  test("Номер хода") {
    startPosition.move((6, 4), (4, 4)).move((1, 4),(3 ,4))
      .move((7, 5),(4, 2)).turn shouldBe 4
  }
}
