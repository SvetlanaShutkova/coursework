name := "play"

import sbt.Keys._
import sbt._
import scala.language.postfixOps

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "-" + module.revision + "." + artifact.extension
}

version in ThisBuild := "0.0.1"

lazy val commonSettings = Seq(
  organization := "com.yourorg",
  scalaVersion := "2.12.9",
  version := "0.0.1",
  resolvers ++= Seq(
    DefaultMavenRepository,
    Resolver.jcenterRepo,
    Resolver.sbtPluginRepo("releases"),
    Resolver.mavenLocal,
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  ),
  scalacOptions ++= CompilerOptions.cOptions,
  test in assembly := {}
)

def baseProject(name: String): Project =
  Project(name, file(name))
    .settings(commonSettings: _*)
    .configs(IntegrationTest)
    .settings(Defaults.itSettings)

lazy val `play` = (project in file("."))
  .aggregate(`seed-api`, common)

lazy val `seed-api` = baseProject("seed-api")
  .dependsOn(common)
  .settings(
    libraryDependencies ++= Dependencies.seedapi,
    Compile/mainClass := Some("com.yourorg.play.Starter"),
    mainClass in assembly := Some("com.yourorg.play.Starter")
  )
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick" % "3.3.3",
      "org.scalatest" %% "scalatest" % "3.2.0" % Test,
      "org.slf4j" % "slf4j-nop" % "1.6.4",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
      "com.h2database" % "h2" % "1.4.200"
    )
  )
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)


lazy val common = baseProject("common")
  .settings(
    libraryDependencies ++= Dependencies.common
  )

val runServer = inputKey[Unit]("Runs web-server")
val runStart = inputKey[Unit]("Runs web-server")

runServer := {
  (run in Compile in `seed-api`).evaluated
}

